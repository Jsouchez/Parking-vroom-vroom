<?php

namespace App\DataFixtures;

use App\Entity\Rent;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class RentFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {

        $rent = new Rent();
        $rent->setPlace($this->getReference(PlaceFixtures::PLACE_REF));
        $rent->setStartedAt(new DateTime());
        $rent->setFinishedAt(new DateTime('2021-03-29'));
        $rent->setUsedBy($this->getReference(UserFixtures::USER_REF));
        $manager->persist($rent);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            PlaceFixtures::class,
            UserFixtures::class
        ];
    }
}
