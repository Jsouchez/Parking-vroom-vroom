<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class MichelFixtures extends Fixture implements DependentFixtureInterface
{
    public const MICHEL_REF = 'michel-ref';


    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $michel = new User();
        $michel->setFirstName('Michel');
        $michel->setLastName('Le Vrai');
        $michel->setPassword($this->encoder->encodePassword($michel, '123456'));
        $michel->setEmail('michel@vroomvroom.com');
        $michel->addResidence($this->getReference(ResidenceFixtures::RESIDENCE_REF));
        $michel->setIsVerified(true);
        $manager->persist($michel);

        $this->addReference(self::MICHEL_REF, $michel);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            ResidenceFixtures::class,
        ];
    }
}
