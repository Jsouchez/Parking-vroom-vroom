<?php

namespace App\DataFixtures;

use App\Entity\Residence;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ResidenceFixtures extends Fixture
{
    public const RESIDENCE_REF = 'residence-fix';

    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i < 3; $i++) {
            $residence = new Residence();
            $residence->setName('Residence ' . $i);
            $manager->persist($residence);
        }

        $residenceRef = new Residence();
        $residenceRef->setName('Residence de fou');
        $manager->persist($residenceRef);
        $this->addReference(self::RESIDENCE_REF, $residenceRef);
        $manager->flush();
    }
}
