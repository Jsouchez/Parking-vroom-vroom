<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    public const USER_REF = 'user-ref';

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $userRef = new User();
        $userRef->setFirstName('Admin');
        $userRef->setLastName('Admin');
        $userRef->setPassword($this->encoder->encodePassword($userRef, '123456'));
        $userRef->setEmail('admin@vroomvroom.com');
        $userRef->setRoles(['ROLE_ADMIN']);
        $userRef->setIsVerified(true);
        $manager->persist($userRef);

        $this->addReference(self::USER_REF, $userRef);

        for ($i = 1; $i < 11; $i++) {
            $user = new User();
            $user->setFirstName('Michel' . $i);
            $user->setLastName('Dupond');
            $user->setPassword($this->encoder->encodePassword($user, '123456'));
            $user->setEmail('Michel' . $i . '.Dupond@gmail.com');
            $user->setIsVerified(true);
            $manager->persist($user);
        }
        $manager->flush();
    }
}
