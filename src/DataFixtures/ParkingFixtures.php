<?php

namespace App\DataFixtures;

use App\Entity\Parking;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class ParkingFixtures extends Fixture implements DependentFixtureInterface
{
    public const PARKING_REF = 'parking-fix';

    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i < 5; $i++) {
            $parking = new Parking();
            $parking->setName('parking ' . $i);
            $parking->setNbPlaces(50);
            $manager->persist($parking);
        }

        $parkingRef = new Parking();
        $parkingRef->setName('Super Parking');
        $parkingRef->setNbPlaces(50);
        $parkingRef->addResidence($this->getReference(ResidenceFixtures::RESIDENCE_REF));
        $manager->persist($parkingRef);

        $this->addReference(self::PARKING_REF, $parkingRef);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            ResidenceFixtures::class
        ];
    }
}
