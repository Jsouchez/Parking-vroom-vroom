<?php

namespace App\Controller\Admin;

use App\Entity\Parking;
use App\Entity\Place;
use App\Repository\PlaceRepository;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ParkingCrudController extends AbstractCrudController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public static function getEntityFqcn(): string
    {
        return Parking::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL);
    }

    public function configureFields(string $pageName): iterable
    {
        $placeIsUsed = $this->em->getRepository(Place::class)->findBy(['isUsed' => false]);

        $fields = [
            TextField::new('name', 'Nom'),
            NumberField::new('nbPlaces', 'Numbre de place max.'),
            AssociationField::new('places', 'places utilisées')->hideOnForm(),
            // AssociationField::new('places', 'Place disponible')->setQueryBuilder(function ($qb, $context) {
            //     $customer = $context->getEntity()->getInstance();
            //     $qb
            //         ->andWhere('entity.customer = :customer')
            //         ->setParameter('customer', $customer->getId());
            // }),
            AssociationField::new('residences', 'Liste des résidences')->setFormTypeOptions(['by_reference' => false]),
        ];

        return $fields;
    }
}
