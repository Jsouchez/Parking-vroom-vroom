<?php

namespace App\Controller\Admin;

use App\Entity\Residence;
use App\Form\ParkingType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ResidenceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Residence::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL);
    }

    public function configureFields(string $pageName): iterable
    {
        $fields = [
            TextField::new('name', 'Nom'),
            TextField::new('address', 'Adresse postal'),
            AssociationField::new('residents', 'Liste des résidents')->hideOnDetail(),
            CollectionField::new('residents', 'Liste des residents')->setTemplatePath('dashboard/residents.html.twig')->onlyOnDetail(),
            AssociationField::new('parkings', 'Liste des parkings')->setFormTypeOptions(['by_reference' => false])->onlyOnForms(),
            CollectionField::new('parkings', 'Liste des parkings')->setEntryType(ParkingType::class)->onlyWhenCreating(),
            CollectionField::new('parkings', 'Liste des parkings')->setTemplatePath('dashboard/parkings.html.twig')->onlyOnDetail()
        ];

        return $fields;
    }
}
