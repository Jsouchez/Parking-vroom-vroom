<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ResidenceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ResidenceRepository::class)
 */
class Residence
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\ManyToMany(targetEntity=Parking::class, inversedBy="residences", cascade={"persist"}, orphanRemoval=true)
     */
    private $parkings;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="residences")
     */
    private $residents;

    public function __construct()
    {
        $this->parkings = new ArrayCollection();
        $this->residents = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return Collection|Parking[]
     */
    public function getParkings(): Collection
    {
        return $this->parkings;
    }

    public function addParking(Parking $parking): self
    {
        if (!$this->parkings->contains($parking)) {
            $this->parkings[] = $parking;
        }

        return $this;
    }

    public function removeParking(Parking $parking): self
    {
        $this->parkings->removeElement($parking);

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getResidents(): Collection
    {
        return $this->residents;
    }

    public function addResident(User $resident): self
    {
        if (!$this->residents->contains($resident)) {
            $this->residents[] = $resident;
        }

        return $this;
    }

    public function removeResident(User $resident): self
    {
        $this->residents->removeElement($resident);

        return $this;
    }
}
